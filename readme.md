### WebSlurp

A distributed web crawler/downloader.
Utilizes RabbitMQ to distribute the downloading process to a number of workers. 

#### Installation 

Requires the following python libraries

 * pika - communication with rabbitmq
 
   `# pip install pika`
   
 * beautifulsoup4 - parsing and html utility
 
   `# pip install beautifulsoup4 `
   
 * rabbitmq - running on local host with default guest access permission
   
####Description

System consists of 3 parts

##### server.py

Receives commands from the command interface. Builds jobs and keeps tracks of job progress. All job information is stored in memory. Enques download jobs to be performed by the workers.

##### worker.py

Consumes a queue with download tasks. Each task consists of a url to download and job parameters. If the download matches the job description the file is downloaded in the download folder. A large number of workers can be stared to process download jobs. Each worker downloads documents synchronously. If the downloaded document is an html document is scans the html for links extracts them and sends them to the server to be filtered for duplicates and distributed to other workers.

##### webslurp.py

The command line interface to the system. Supports commands to get a list of jobs form the server. Get details for a job and submit a new job.
 

  
 