import json,uuid,pika

class RpcClient(object):
    def __init__(self, exchange, routing_key, connection):
        self.exchange = exchange
        self.connection = connection
        self.channel = connection.channel()
        self.routing_key = routing_key
        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = json.loads(body)

    def call(self, req):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange=self.exchange,
                                   routing_key=self.routing_key,
                                   properties=pika.BasicProperties(
                                         reply_to = self.callback_queue,
                                         correlation_id = self.corr_id,
                                         ),
                                   body=json.dumps(req))
        while self.response is None:
            self.connection.process_data_events()
        return self.response
