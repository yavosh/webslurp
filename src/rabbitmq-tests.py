import pika
import json

def on_message(method_frame, header_frame, body):
    if method_frame:
        print method_frame
        print header_frame
        print body

        channel.basic_ack(method_frame.delivery_tag)
    else:
        print 'No message returned'

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.basic_qos(prefetch_count=1)

channel.exchange_declare(exchange='webslurp', type='topic')
channel.queue_declare(queue='webslurp.everything', durable=True)
channel.queue_bind(exchange='webslurp',
                       queue="webslurp.everything",
                       routing_key="#")


message = json.dumps({"node": "1", "role": "client", "status": "idle"})

channel.basic_publish(exchange='webslurp',
                      routing_key='webslurp.status.1',
                      body=message)

channel.basic_get(callback=on_message, queue='webslurp.everything')


print " [x] Sent %r" % (message,)
connection.close()

#result = channel.queue_declare(exclusive=True)
