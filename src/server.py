import json,pika,uuid
from config import *
from lib import *

#open channel
channel = connection.channel()
setup_queues(channel)

# Make sure we get fair distribution of messages
channel.basic_qos(prefetch_count=1)

jobs = {}

# Define the client status message queue and bind to webslurp.status.client topic
result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange=RMQ_EXCHANGE_NAME,
                       queue=queue_name,
                       routing_key="webslurp.status.worker")

def on_client_callback(ch, method, props, body) :
    #print " [x] %r:%r" % (method.routing_key, body,)
    ch.basic_ack(delivery_tag = method.delivery_tag)
    response = json.loads(body)

    job = jobs.get(response.get("job_id", ""), None)
    current_depth = response.get("depth", 0)

    if job is None:
        print " [x] Error invalid job id %r" % response
        return

    if response["result"] == "error":
        job["urls_error"] = job.get("urls_error",0) + 1

    if response["result"] == "download":
        job["files"].append(response["file"])
        job["urls_processed"] = job.get("urls_processed",0) + 1

    if response["result"] == "urls":
        job["urls_processed"] += 1

        if current_depth + 1 < job.get("max_depth", 0) :
            # filter urls already processed to avoid circular loops
            # and to avoid duplicate processing due to missing trailing slashes and such
            filtered_urls = [scrub_url(u) for u in response["urls"]]
            filtered_urls = [u for u in filtered_urls if u not in job["urls"] ]
            filtered_urls = set(filtered_urls)

            print "Response URLS %r" % response["urls"]
            print "Filtered URLS %r" % filtered_urls

            for u in filtered_urls:
                print "Send URL %r for processing" % u
                # Prepare worker request
                req = {
                    "command": "download",
                    # Copy depth from worker and increment by one
                    "depth": response.get("depth", 0) + 1
                }

                for job_key in ['job_id', 'max_depth', 'download_ext', 'download_type']:
                    req[job_key] = job[job_key]
                req["url"] = u

                # Add url to job urls to avoid duplicates
                job["urls"].append(u)
                job["urls_total"] += 1
                publish_pmessage(channel, req, RMQ_EXCHANGE_NAME, 'webslurp.job.download')
        else:
            print " [x] Max depth reached"

def on_server_status(ch, method, props, body):
    print " [x] %r:%r" % (method.routing_key, body,)
    request = json.loads(body)
    print request

    # Get command from request data
    command = request.get("command", "")
    response = None

    if command == "status":
        response = json.dumps({"status": "ok", "jobs":jobs.keys(),})

    if command == "job_status":
        print "Job ID: %s" % request.get("job_id", "")
        job = jobs.get(request.get("job_id", ""), None)
        if job is None:
            response = json.dumps({"status": "error", "message": "Invalid job id"})
        else:
            response = json.dumps({"status": "ok", "job": job})

    if command == "job":
        print "New Job filetype: %s url: %s" % (request.get("filetype", ""), 
                                                request.get("url", ""))

        job_url = request.get("url", "")
        job_url = scrub_url(job_url)
        job_filetype = request.get("filetype", "")

        error = ""
        if not is_valid_url(job_url):
            error = "Invalid url %s" % job_url

        if MIME_TYPES_INV.get(job_filetype, "") == "":
            error = "Invalid filetype %s" % job_filetype + " " + error 

        if error == "":
            # Create new job dictionary
            job = {
                "job_id": str(uuid.uuid4()),
                "urls": [],
                "files": [],
                "urls_processed": 0,
                "urls_total": 0,
                "urls_error": 0,
                "max_depth": 5,
                "url": job_url,
                "download_ext": job_filetype,
                "download_type": MIME_TYPES_INV.get(job_filetype, ""),
            }

            # Add to jobs collection
            jobs[job["job_id"]] = job

            # Prepare worker request
            req = {
                "command": "download",
                "depth": 0,
            }

            for job_key in ['job_id', 'max_depth', 'url', 'download_ext', 'download_type']:
                req[job_key] = job[job_key] 

            # Add the url to the url excusion list
            job["urls"].append(req["url"])
            # Publish message for worked
            publish_pmessage(channel, req, RMQ_EXCHANGE_NAME, 'webslurp.job.download')
            # Send ok and new job id
            response = json.dumps({"status": "ok", 
                                "job_id": job["job_id"]})
        else :
            response = json.dumps({"status": "error", 
                                "message": "Error: %s " % error})
    if response is None:
        # No response was generated, generate default
        response = json.dumps({"status": "error", 
                                "message": "Invalid command '%s' " % command})


    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                     props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag = method.delivery_tag)

# Consume job messages from jobs queue
channel.basic_consume(on_client_callback,
                      queue=queue_name,
                      no_ack=False) # We want manual acknowledgements

# Consume server status requests
channel.basic_consume(on_server_status, queue='webslurp.server.jobs')

print ' [*] Job server running. To exit press CTRL+C'

try:
    channel.start_consuming()
# Catch a Keyboard Interrupt to make sure that the connection is closed cleanly
except KeyboardInterrupt:
    channel.stop_consuming()

# Gracefully close the connection
connection.close()
print ' [*] Job server stopped.'
