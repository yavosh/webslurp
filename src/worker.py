import os,json,uuid,urlparse,urllib2
from config import *
from lib import *

node_id = str(uuid.uuid4())

#open channel
channel = connection.channel()
setup_queues(channel)

# Make sure we get fair distribution of messages
channel.basic_qos(prefetch_count=1)

def download_file(url, response, job_id):
    try:
        parent_url = urlparse.urlparse(url)
        dir_name = DOWNLOAD_DIR + job_id + "/"
        # Create downloads directory if it does not exist
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        file_name = dir_name + parent_url.path.replace("/", "_")
        f = open(file_name, 'wb')
        f.write(response.read())
        f.close()
        return "ok", file_name
    except Exception, e:
        print " [*] Error downloading file : %s" % e
        return "error", ""
        

def job_callback(ch, method, properties, body):
    #print " [*] %r:%r" % (method.routing_key, body,)
    job = json.loads(body)

    url = job['url']
    print " [*] Trying to download %r" % url

    # Get content of url
    try:
        response = urllib2.urlopen(url)
        response_info = response.info()
        if int(response_info.get('Content-Length', "0")) > MAX_CONTENT_LENGTH:
            print "Content-Length: %s" % response_info['Content-Length'] 
            ch.basic_ack(delivery_tag = method.delivery_tag)
            print "[*] Error getting url %r - too large" % url
            req = {"node": node_id, "result": "error", "result_details":"too-large", "url": url, "depth": job["depth"], "job_id": job["job_id"]}
            publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")
            return
    except urllib2.URLError:
        ch.basic_ack(delivery_tag = method.delivery_tag)
        print "[*] URL Error getting url %r" % url
        req = {"node": node_id, "result": "error", "result_details":"url-error", "url": url, "depth": job["depth"], "job_id": job["job_id"]}
        publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")
        return

    # if content type of url matches the download type
    # try to download the file to the download dir
    # and send status to server

    parsed_url = urlparse.urlparse(url)
    extension = os.path.splitext(parsed_url.path)[1]


    if response_info.gettype() == job["download_type"] or extension == job["download_ext"]:
        print " [x] Downloading url %s" % url
        status, file_name = download_file(url, response, job["job_id"])
        print " [x] Downloading url done %s status %s" % (url, status)

        if status == "ok":
            req = {"node": node_id, "result": "download", "result_details": "error-downloading", "url": url, "file": file_name, "depth": job["depth"], "job_id": job["job_id"]}
        else:
            req = {"node": node_id, "result": "error", "url": url, "file": file_name, "depth": job["depth"], "job_id": job["job_id"]}
        publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")

    # if we have an html page extract links and send to server
    if response_info.gettype() == "text/html":
        print " [x] Downloading html %s" % url

        try:
            # Get content of url
            raw_html = response.read()
            # Get all urls in the html
            child_urls = extract_urls(url, raw_html)
            # Filter urls which belong to this server
            same_urls = filter_urls(url, child_urls)
            req = {"node": node_id, "result": "urls", "urls": same_urls, "depth": job["depth"], "job_id": job["job_id"]}
            publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")
            print "Send %d urls to server " % len(same_urls)
        except Exception, e:
            req = {"node": node_id, "result": "error", "url": url, "file": file_name, "depth": job["depth"], "job_id": job["job_id"]}
            publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")
            print " [*] Error downloading url : %s" % e

    else:
        req = {"node": node_id, "result": "done", "depth": job["depth"], "job_id": job["job_id"]}
        publish_message(ch, req, RMQ_EXCHANGE_NAME, "webslurp.status.worker")

    ch.basic_ack(delivery_tag = method.delivery_tag)
    

# Consume job messages from jobs queue
channel.basic_consume(job_callback,
                      queue="webslurp.jobs",
                      no_ack=False) # We want manual acknowledgements


print ' [*] Waiting for jobs. To exit press CTRL+C'
try:
    channel.start_consuming()
# Catch a Keyboard Interrupt to make sure that the connection is closed cleanly
except KeyboardInterrupt:
    channel.stop_consuming()

# Gracefully close the connection
connection.close()
print ' [*] Worker stopped.'
