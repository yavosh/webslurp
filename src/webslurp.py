#!/usr/bin/python

import sys, getopt
import RpcClient
from config import *

def help():
    print 'Web slurp: download files '
    print '# webslurp.py -u <url> -f <filetype>'
    print ''
    print 'Web slurp: server status '
    print '# webslurp.py -s'
    print ''
    print 'Web slurp: server job status '
    print '# webslurp.py -j <jobid>'
    print ''
    print 'Example - download all pdf files by crawling the website'
    print '# webslurp.py -u http://example.com -f pdf'

def print_server_error(resp):
    print "********************************************"
    print "  Server Error: " + resp.get("message", "")
    print "********************************************"


def print_server_status(resp):
    if resp.get("status") != "ok":
        print print_server_error(resp)
        return

    print "********************************************"
    print "  Server Status (%d) jobs" % len(resp.get("jobs", []))
    print ""
    for job_id in resp.get("jobs", []):
        print "  %s" % job_id

    print "********************************************"

def print_job_status(resp):
    if resp.get("status") != "ok":
        print print_server_error(resp)
        return

    job = resp.get('job', {})
    print "********************************************"
    print "  Job Id (%s) " % job.get("job_id", "")
    print ""
    print "  URL           : %s" % job.get("url", "")
    print "  File Type     : %s" % job.get("download_ext", "")
    print "  File Content  : %s" % job.get("download_type", "")
    print "  Urls total    : %d" % job.get("urls_total", 0)
    print "  Urls done     : %d" % job.get("urls_processed", 0)
    print "  Urls error    : %d" % job.get("urls_error", 0)
    print "  Downloads     : %d" % len(job.get("files", []))
    print ""
    print "Files"
    for filename in job.get("files", []):
        print "  %s" % filename
    print ""
    print "URLS"
    url_counter = 0
    url_max_display = 100
    for url in job.get("urls", []):
        url_counter += 1
        print "  %s" % url
        if url_counter>url_max_display:
            print "... too many urls, showing first %d" % url_max_display
            break
    print "********************************************"

def main(argv):
    filetype = ""
    url = ""
    server_status = False
    jobid = ""

    try:
        opts, args = getopt.getopt(argv,"hsu:f:j:",
            ["help","status","job=","filetype=","url="])
    except getopt.GetoptError as err:
        print str(err)
        help()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            help()
            sys.exit()
        elif opt in ("-u", "--url"):
            url = arg
        elif opt in ("-f", "--filetype"):
            filetype = arg
        elif opt in ("-j", "--job"):
            jobid = arg
        elif opt in ("-s", "--status"):
            server_status = True

    client = RpcClient.RpcClient(RMQ_EXCHANGE_NAME, "webslurp.server", connection)

    if server_status:
        req = {"command": "status"}
        resp = client.call(req)
        #print "Server response: %r" % resp
        #print ""
        print_server_status(resp)
        return

    if jobid != "":
        req = {"command": "job_status", "job_id": jobid}
        resp = client.call(req)
        #print "Server response: %r" % resp
        print_job_status(resp)
        return

    if url != "" and filetype != "":
        req = {"command": "job", "filetype": filetype, "url" : url}
        resp = client.call(req)
        print "Server response: %r" % resp
        return

    # Show usage
    help()


if __name__ == "__main__":
    main(sys.argv[1:])

