import pika, os

RMQ_EXCHANGE_NAME="webslurp"
DOWNLOAD_DIR="./downloads/"
MAX_CONTENT_LENGTH=1024*1024*1024*5 # 5MB

MIME_TYPES = {
    "application/pdf":  "pdf",
    "image/gif":        "gif",
    "image/jpeg":       "jpeg",
}

MIME_TYPES_INV={ v: k for (k,v) in MIME_TYPES.items()}


# Create downloads directory if it does not exist
if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

def setup_queues(channel):
    """Create exchange and queues"""

    # Define the exchange
    channel.exchange_declare(exchange=RMQ_EXCHANGE_NAME, type='topic')

    # Define the jobs queue and bind to it 
    channel.queue_declare(queue='webslurp.jobs', durable=True)
    channel.queue_declare(queue='webslurp.server.jobs', durable=True)

    channel.queue_bind(exchange=RMQ_EXCHANGE_NAME,
                           queue="webslurp.jobs",
                           routing_key="webslurp.job.#")

    channel.queue_bind(exchange=RMQ_EXCHANGE_NAME,
                           queue="webslurp.server.jobs",
                           routing_key="webslurp.server")

