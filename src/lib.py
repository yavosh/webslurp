import re
import urlparse
import json
import pika
from bs4 import BeautifulSoup

def is_valid_url(url):
    parsed_url=urlparse.urlparse(url)
    if parsed_url.scheme == "":
        return False
    if parsed_url.netloc == "":
        return False
    if parsed_url.scheme != "http" and parsed_url.scheme != "https":
        return False
    return True 

def apply_base_url(base_url, url_string):
    """Apply base url to the current url only if the url is not absolute"""
    return urlparse.urljoin(base_url, url_string)

def extract_urls_regex(html):
    """Extract all hrefs from the current html segment"""
    hrefs = re.findall(r'href=[\'"]?([^\'" >]+)', html)
    return hrefs

def extract_imgs_regex(html):
    """Extract all img src from the current html segment"""
    imgs = re.findall(r'src=[\'"]?([^\'" >]+)', html)
    return imgs

def extract_urls(base_url, html):
    """Extract all hrefs from the current html segment using BeautifulSoup"""
    try:
        soup = BeautifulSoup(html)
        all_urls = []
        for tag in soup.findAll('a', href=True):
            url = apply_base_url(base_url, tag['href'])
            all_urls.append(url)
        return all_urls
    except ValueError:
        return []

def extract_imgs(base_url, html):
    """Extract all img src from the current html segment using BeautifulSoup"""
    try:
        soup = BeautifulSoup(html)
        all_img_urls = []
        for tag in soup.findAll('img', src=True):
            url = apply_base_url(base_url, tag['src'])
            all_img_urls.append(url)
        return all_img_urls
    except ValueError:
        return []

def scrub_url(url):
    url=url.split("#")[0]
    parsed_url = urlparse.urlparse(url)
    # make sure we have a trailing slash
    if parsed_url.path == "":
        url += "/"
    return url

def filter_urls(base_url, urls):
    #mailto:
    """
    Return urls where the domain name matches the base_url domain name
    and perform cleanup of the urls
    """

    urls = [x for x in urls if urlparse.urlparse(base_url).netloc == urlparse.urlparse(x).netloc]
    urls = [x for x in urls if urlparse.urlparse(base_url).scheme != 'mailto']
    return urls

def publish_pmessage(ch, data, exchange, routing_key):
    """Publish persistent message from a dictionary"""
    message = json.dumps(data)
    ch.basic_publish(exchange=exchange,
                      routing_key=routing_key,
                      body=message,
                      properties=pika.BasicProperties(
                        content_type="application/json",
                        delivery_mode = 2, # make message persistent
                      )
                    )

def publish_message(ch, data, exchange, routing_key):
    """Publish message from a dictionary"""
    message = json.dumps(data)
    ch.basic_publish(exchange=exchange,
                      routing_key=routing_key,
                      body=message,
                      properties=pika.BasicProperties(
                        content_type="application/json",
                      )
                    )